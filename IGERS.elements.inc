<?php

/**
 * Implements hook_element_info().
 *
 */
function IGERS_element_info() {
  //instagram_media_id
  //instagram_timestamp
  //instagram_user_id
  $type = array(
    'instagram_user_id' => array(
      '#input' => true,
      '#theme' => array('textfield'),
      '#autocomplete_path' => 'instagram-suite/autocomplete/instagram-user-id',
      '#theme_wrappers' => array('form_element'),
      '#process' => array('instagram_user_id_element_process'),
      '#element_validate' => array('instagram_user_id_element_validate'),
    ),
    'instagram_timestamp' => array(
      '#input' => true,
      '#theme' => array('textfield'),
      '#theme_wrappers' => array('form_element'),
      '#autocomplete_path' => FALSE,
      '#process' => array('instagram_timestamp_process'),
      '#element_validate' => array('instagram_timestamp_element_validate'),
    ),
    'instagram_tag' => array(
      '#input' => true,
      '#theme' => array('textfield'),
      '#autocomplete_path' => 'instagram-suite/autocomplete/instagram-tag',
      '#theme_wrappers' => array('form_element'),
    ),
  );
  return $type;
}
function instagram_timestamp_element_validate($element,&$form_state){
  form_set_value($element, strtotime($element['#value']), $form_state);
}
function instagram_timestamp_process($element,$form_state,$complete_form){
  if($element['#value']){
    $input = drupal_array_get_nested_value($form_state['input'], $element['#parents']);
    if($input){
      $timestamp = strtotime($input);
    }
    else{
      $timestamp = $element['#default_value'];
    }
    $element['#value'] = $element['#default_value'] = date("Y/m/d H:i:s",$timestamp);
  }

  return $element;
}
function instagram_user_id_element_validate($element,&$form_state){
  preg_match('/^(?:\s*|(.*) )?\[\s*(\d+)\s*\]$/', $element['#value'], $matches);
  watchdog('IGERS', $element['#value']);
  if($matches){
    $user_name = $matches[1];
    $user_id = $matches[2];
  }
  $user_name_parents = $element['#parents'];
  $user_name_parents[count($user_name_parents)-1] = 'user_name';
  form_set_value($element, $user_id, $form_state);

  drupal_array_set_nested_value($form_state['values'], $user_name_parents, $user_name, TRUE);
  drupal_array_get_nested_value($form_state['values'], $user_name_parents, $user_name, TRUE);  
}

function instagram_user_id_element_process($element,$form_state,$complete_form){
  if(!$form_state['input']){
    $field_parents = $element['#parents'];
    $field_parents[count($field_parents)-1] = 'value';
  
    $nodeArray = (array) $form_state['node'];
    $field_value = unserialize(drupal_array_get_nested_value($nodeArray, $field_parents));
  
    if($element['#value']){
      $element['#value'] = $element['#default_value'] = $field_value['user_name'] . ' [' . $element['#value'] . ']';
    }
  }

  return $element;  
}