<div class="instagram-suite-box">
  <?php 
    print $processed_images->thumb_image;
  ?>  
    
  <div class="instagram-suite-info">
    <div class="author">
      by 
      <?php print theme('imagecache_external', array('path' => $user->profile_picture, 'style_name'=> 'thumbnail')); ?></php>
      <b><?php print $user->username; ?></b>
    </div>  
    <div class="likes">
      <span><?php print $likes->count; ?>  </span>
    </div>
    <div class="comments">
      <span><?php print $comments->count; ?>  </span>
    </div>
  </div>
</div>