<?php

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function IGERS_field_info() {
  return array(
    'instagram_image' => array(
      'label' => t('Instragram'),
      'description' => t('This field insert an instagram gallery'),
      'default_widget' => 'tag_recent_media',
      'default_formatter' => 'IGERS_simple_text',
    ),
  );
}

/**
 * Implements hook_field_validate().
 *
 * @see IGERS_field_widget_error()
 */
function IGERS_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  watchdog('items validate', print_r($items, TRUE));     
  foreach ($items as $delta => $item) {
    /** @TODO check the input  
    
    if (!empty($item['rgb'])) {
      if (! preg_match('@^#[0-9a-f]{6}$@', $item['rgb'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'IGERS_invalid',
          'message' => t('Color must be in the HTML format #abcdef.'),
        );
      }
    }
    */
  }
}




/**
 * Implements hook_field_is_empty().
 *
 * This function check if we have a selected steam type and a not empty streamtag.
 */
function IGERS_field_is_empty($item, $field) {
  return FALSE;
  if(empty($item['streamtype'])){
    return TRUE;
  } else {
    return ( empty($item['streamtag']) )? TRUE : FALSE;
  }
  
}


/**
 * Implements hook_field_formatter_info().
 *
 * @see IGERS_field_formatter_view()
 */
function IGERS_field_formatter_info() {
  return array(
    // This formatter displays the images gallery.
    'instagram_gallery' => array( 
      'label' => t('Instagram Gallery'),
      'field types' => array('instagram_image'), 
      'settings'  => array( 
        'thumb_style' => 'default',                 // Thumbnails style
        'image_style' => 'default',                 // Lightboxed immages style
        'style' => 'instagram_classic',
        'infinite_scroll' => true
      )
    ),
    'simple_instagram_gallery' => array( 
      'label' => t('Image Gallery'),
      'field types' => array('instagram_image'), 
      'settings'  => array( 
        'thumb_style' => 'default',                 // Thumbnails style
        'image_style' => 'default',                 // Lightboxed immages style
      )
    )
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 * 
 */
function IGERS_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $element = array();
  if(   $field['type'] == 'instagram_image' 
        && isset($instance['display'][$view_mode]) 
        && $instance['display'][$view_mode]['type'] == 'instagram_gallery'){
    
    //This gets the actual settings
    $settings = $instance['display'][$view_mode]['settings'];

    //Initialize the element variable
    $element = array();
    $entity_info = entity_get_info('file');
    
    // Retrive the media presets from image module
    $image_styles = image_styles();
    foreach ($image_styles as $key => $image_style) {
      $options[$key] = $image_style['name'];
    }
  
    //Add your select box
  
    $element['thumb_style'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Thumbnails size'),                   // Widget label
      '#description'    => t('Select what size of thumbnails image'), // Helper text
      '#default_value'  => $settings['thumb_style'],              // Get the value if it's already been set
      '#options'        => $options,
    );
    $element['image_style'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Image size'),                   // Widget label
      '#description'    => t('Select what size of image'), // Helper text
      '#default_value'  => $settings['image_style'],              // Get the value if it's already been set
      '#options'        => $options,
    );
    $element['style'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Style'),                   // Widget label
      '#description'    => t('Select a style'), // Helper text
      '#default_value'  => $settings['css_style'],              // Get the value if it's already been set
      '#options'        => array('instagram_classic'=>'Instagram Classic', 'instagram_modern'=>'Instagram Modern'),
    );
    $element['infinite_scroll'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Infinite scroll'),                   // Widget label
      '#description'    => t('Enable or disable infinite scroll'), // Helper text
      '#default_value'  => $settings['infinite_scroll'],              // Get the value if it's already been set
      '#options'        => array(1 => t('Yes'), 0 => t('No')),
    );
    
    
   
  } else if(   $field['type'] == 'instagram_image' 
        && isset($instance['display'][$view_mode]) 
        && $instance['display'][$view_mode]['type'] == 'simple_instagram_gallery'){
    
    //This gets the actual settings
    $settings = $instance['display'][$view_mode]['settings'];

    //Initialize the element variable
    $element = array();
    $entity_info = entity_get_info('file');
    
    // Retrive the media presets from image module
    $image_styles = image_styles();
    foreach ($image_styles as $key => $image_style) {
      $options[$key] = $image_style['name'];
    }
  
    //Add your select box
  
    $element['thumb_style'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Thumbnails size'),                   // Widget label
      '#description'    => t('Select what size of thumbnails image'), // Helper text
      '#default_value'  => $settings['thumb_style'],              // Get the value if it's already been set
      '#options'        => $options,
    );
    $element['image_style'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Image size'),                   // Widget label
      '#description'    => t('Select what size of image'), // Helper text
      '#default_value'  => $settings['image_style'],              // Get the value if it's already been set
      '#options'        => $options,
    );
  }
  
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 * 
 * composizione del sommario dei setting
 */
function IGERS_field_formatter_settings_summary($field, $instance, $view_mode) {
  $summary = '';
  if(   $field['type'] == 'instagram_image' 
        && isset($instance['display'][$view_mode]) 
        && $instance['display'][$view_mode]['type'] == 'instagram_gallery'){
    $display = $instance['display'][$view_mode];
    $settings = $display['settings'];
    $summary = t('Use a \'@thumb\' Thumbs style, \'@image\' image style with thumbnail list', array(
            '@thumb'     => $settings['thumb_style'],
            '@image'  => $settings['image_style']
          )); 
    
    
    
  } else if(   $field['type'] == 'instagram_image' 
        && isset($instance['display'][$view_mode]) 
        && $instance['display'][$view_mode]['type'] == 'simple_instagram_gallery'){
    $display = $instance['display'][$view_mode];
    $settings = $display['settings'];
    $summary = t('Use a \'@thumb\' Thumbs style, \'@image\' image style with thumbnail list', array(
            '@thumb'     => $settings['thumb_style'],
            '@image'  => $settings['image_style']
          )); 
  }
  return $summary;
}



/**
 * Implements hook_field_formatter_view().
 *
 * One formatters are implemented.
 * - instagram_gallery 
 *
 * @see IGERS_field_formatter_info()
 */
function IGERS_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    // This formatter simply outputs the field as text and with a color.
    case 'instagram_gallery':
      
      $settings = $display['settings'];
      
      if( $settings['style'] != 'custom'){
        drupal_add_css(drupal_get_path('module','IGERS') . '/css/'. $settings['style'] .'.css');
        $template_file = drupal_get_path('module', 'IGERS') . '/templates/' . $settings['style'] . '.tpl.php';
      } 
      
      foreach ($items as $delta => $item) {
        $item = unserialize($item['value']);
        
        $element[$delta] = array(
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => array('class' => 'IGERS-gallery ' . str_replace("_","-",$settings['style']))
        );
        $method = $instance['widget']['type'];
        $kwargs = $item;
        
        $gallery = IGERS_make_request($method,$kwargs);

        $settings['field_name'] = $field['field_name'];
        $element[$delta]['#value'] = '<div class="IGERS-item-container">';
        $element[$delta]['#value'] .= theme('IGERS_item', array('gallery' => $gallery, 'settings' => $settings));
        $element[$delta]['#value'] .= '</div><div class="clear"></div>';
        
        if($settings['infinite_scroll'] && !empty($gallery['next_url'])){
          drupal_add_js(drupal_get_path('module','IGERS') . '/IGERS.js');
          $element[$delta]['#value'] .= l('<span>' . t('Load next') . '</span>','IGERS/field/next/'.$gallery['cid'].'/'.$entity_type.'/'.$instance['bundle'].'/'.$instance['field_name'],array('html' => true, 'absolute' => true, 'attributes' => array('class' => 'IGERS-infinite-scroll')));
        }

      }
    break;
    
    case 'simple_instagram_gallery':
      
      $settings = $display['settings'];
      
      foreach ($items as $delta => $item) {
        $item = unserialize($item['value']);
        
        $element[$delta] = array(
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => array('class' => 'IGERS-simple-gallery ')
        );
        $method = $instance['widget']['type'];
        $kwargs = $item;
        
        $gallery = IGERS_make_request($method,$kwargs);

        $settings['field_name'] = $field['field_name'];
        $element[$delta]['#value'] = '<div class="IGERS-item-container">';
        $element[$delta]['#value'] .= theme('IGERS_item', array('gallery' => $gallery, 'settings' => $settings));
        $element[$delta]['#value'] .= '</div><div class="clear"></div>';
        
        if( isset($settings['infinite_scroll']) && $settings['infinite_scroll'] && !empty($gallery['next_url'])){
          drupal_add_js(drupal_get_path('module','IGERS') . '/IGERS.js');
          $element[$delta]['#value'] .= l('<span>' . t('Load next') . '</span>','IGERS/field/next/'.$gallery['cid'].'/'.$entity_type.'/'.$instance['bundle'].'/'.$instance['field_name'],array('html' => true, 'absolute' => true, 'attributes' => array('class' => 'IGERS-infinite-scroll')));
        }

      }
    break;
      
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 *
 * @see IGERS_field_widget_form()
 */
function IGERS_field_widget_info() {
    $widgets = array();
    /*
    $widgets['IGERS_default_widget'] = array(
        'label' => t('Default widget'),
        'field types' => array('instagram_image'),
    );
    */
    $widgets['user_recent_media'] = array(
        'label' => t('User recent media'),
        'field types' => array('instagram_image'),
    );
    $widgets['tag_recent_media'] = array(
        'label' => t('Tag recent media'),
        'field types' => array('instagram_image'),
    );

    /*
    $widgets['location_recent_media'] = array(
      'label' => t('Tag recent media'),
      'field types' => array('instagram_image'),
    ); */
  
  return $widgets;
}


/**
 * Implements hook_field_widget_form().
 */
function IGERS_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  switch ($instance['widget']['type']) {
    case 'user_recent_media':
        $value = drupal_array_get_nested_value( $items, array($delta,'value') );
        ($value)? $value = unserialize($value) : 0;
        
        $element += array(
            '#type' => 'fieldset',
            '#element_validate' => array('IGERS_field_widget_validate')
        );

        $element['user_id'] = array(
            '#title' => t('Instagram username'),
            '#type' => 'instagram_user_id',
            '#default_value' => ($value && isset($value['user_id'])) ? $value['user_id'] : '',
          /*  '#required' => TRUE */
        );

        $element['min_timestamp'] = array(
            '#title' => t('From date'),
            '#type' => 'instagram_timestamp',
            '#description' => t('Es. 2014/12/31 22:30:00'), 
            '#default_value' => ($value && isset($value['min_timestamp'])) ? $value['min_timestamp'] : '',
        );

        $element['max_timestamp'] = array(
            '#title' => t('Until date'),
            '#type' => 'instagram_timestamp',
            '#description' => t('Es. 2014/12/31 22:30:00'), 
            '#default_value' => ($value && isset($value['max_timestamp'])) ? $value['max_timestamp'] : '',
        );

        $element['count'] = array(
            '#title' => t('Count of media to return'),
            '#type' => 'textfield',
            '#default_value' => ($value && isset($value['count'])) ? $value['count'] : '',
        );
        break;
 
    case 'tag_recent_media':
        $value = drupal_array_get_nested_value( $items, array($delta,'value') );
        ( $value )? $value = unserialize($value) : 0;
        
        $element += array(
            '#type' => 'fieldset',
            '#element_validate' => array('IGERS_field_widget_validate')
        );

        $element['tag_name'] = array(
            '#title' => t('Tag to search'),
            '#type' => 'instagram_tag',
            '#description' => t('Insert tag to search without the \'#\' in front of it'),  
            '#default_value' => ($value && isset($value['tag_name'])) ? $value['tag_name'] : '',
          /*  '#required' => TRUE */
        );
        break;
  }
  return $element;
}


/**
 * Implements hook_field_widget_error().
 *
 * @see IGERS_field_validate()
 * @see form_error()
 */
function IGERS_field_widget_error($element, $error, $form, &$form_state) {
  /*
  switch ($error['error']) {
    case 'IGERS_invalid':
      form_error($element, $error['message']);
      break;
  }
  */
}

function IGERS_field_widget_validate($element, &$form_state) {
  $input = drupal_array_get_nested_value($form_state, array('values',$element['#field_name'],$element['#language'],$element['#delta']));
  form_set_value($element,array('value' => serialize($input)),$form_state);
}
